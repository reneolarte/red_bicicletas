const mongoose = require('mongoose')

const Bicicleta = require('../../models/bicicleta');

describe('testing Bicicletas', function(){
  beforeEach(function(done) {
    var mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB)
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error'))
    db.once('open', function(){
      console.log('we are coneted to test database');
      done();
    });
  });

  afterEach(function(done){
    Bicicleta.deleteMany({}, function(err, success){
      if (err) console.log(err);
      var mongoDB = 'mongodb://localhost/testdb';
      mongoose.disconnect(mongoDB)
      done();
    })
  })

  
  describe('Bicicleta.createInstance', ()=> {
    it('crea una instancia de bicicleta', ()=>{
      var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1])

      expect(bici.code).toBe("1");
      expect(bici.color).toBe("verde");
      expect(bici.modelo).toBe("urbana");
      expect(bici.ubicacion[0]).toEqual(-34.5);
      expect(bici.ubicacion[1]).toEqual(-54.1);
    })
  });

  describe('Bicicleta.allBicis', ()=>{
    it('comienza vacia', (done)=>{
      Bicicleta.allBicis(function(err, bicis){
        expect(bicis.length).toBe(0);
        done();
      })
    })
  })

  

  describe('Bicicleta.add', ()=>{
    it('agrega una sola bici', (done)=>{
      var aBici = new Bicicleta({code:1, color: "verde", modelo:"urbana"});
      Bicicleta.add(aBici, function(err, newBici){
        if (err) console.log(err);
        Bicicleta.allBicis(function(err, bicis){
          expect(bicis.length).toBe(1);
          expect(bicis[0].code).toEqual(aBici.code);
          done();
        })
      })
    })
  })

  describe('Bicicleta.findByCode', ()=>{
    it('devuelve bicicleta por codigo', (done)=>{
      Bicicleta.allBicis(function(err, bicis){
        expect(bicis.length).toBe(0)

        var aBici = new Bicicleta({code:1, color: "verde", modelo:"urbana"})
        Bicicleta.add(aBici, function(err, newBici){
          if (err) console.log(err);
  
          var bBici = new Bicicleta({code:2, color: "rojo", modelo:"montaña"})
          Bicicleta.add(aBici, function(err, newBici){
            if (err) console.log(err);
            Bicicleta.findByCode(1, function(err, targetBici){
              expect(targetBici.code).toBe("1");
              expect(targetBici.color).toBe("verde");
              expect(targetBici.modelo).toBe("urbana");
              done();

            })
           
          })
        })

      })
    })
  })

  describe('Bicicleta.deleteByCode', ()=>{
    it('devuelve bicicleta por codigo', (done)=>{
      Bicicleta.allBicis(function(err, bicis){
        expect(bicis.length).toBe(0)

        var aBici = new Bicicleta({code:1, color: "verde", modelo:"urbana"})
        Bicicleta.add(aBici, function(err, newBici){
          if (err) console.log(err);
  
          var bBici = new Bicicleta({code:2, color: "rojo", modelo:"montaña"})
          Bicicleta.add(aBici, function(err, newBici){
            if (err) console.log(err);
            Bicicleta.removeByCode({code: 1}, function(err, bicis){
              Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(1)
                done();
              })
            })
           
          })
        })

      })
    })
  })





})





/* test modelo sin persistencia*/
// beforeEach(()=> {Bicicleta.allBicis = []; });

// describe('Bicicleta.allBicis', ()=>{
//   it('comienza vacia', () => {
//     expect(Bicicleta.allBicis.length).toBe(0);
//   });
// });

// describe('Bicicleta.add', ()=>{
//   it('agregamos una', () => {
//     expect(Bicicleta.allBicis.length).toBe(0);

//     var a = new Bicicleta(1, 'rojo', 'urbana', [6.235234, -75.572034]);
//     Bicicleta.add(a)

//     expect(Bicicleta.allBicis.length).toBe(1);
//     expect(Bicicleta.allBicis[0]).toBe(a);
    

//   });
// });


// describe('Bicicleta.findById', ()=>{
//   it('retorna bici id 1', () => {
//     expect(Bicicleta.allBicis.length).toBe(0);

//     var aBici1 = new Bicicleta(1, 'rojo', 'urbana', [6.235234, -75.572034]);
//     var aBici2 = new Bicicleta(2, 'azul', 'montaña', [6.235235, -75.572035]);

//     Bicicleta.add(aBici1);
//     Bicicleta.add(aBici2);

//     var testBici1 = Bicicleta.findById(1);
    
//     expect(testBici1.id).toBe(1);
//     expect(testBici1.color).toBe(aBici1.color);
    
//   });
// });

// describe('Bicicleta.findRemoveId', ()=>{
//   it('retira una bici por id', () => {
//     expect(Bicicleta.allBicis.length).toBe(0);

//     var aBici1 = new Bicicleta(1, 'rojo', 'urbana', [6.235234, -75.572034]);
    
//     Bicicleta.removeById(1);
    
//     expect(Bicicleta.allBicis.length).toBe(0);

    
//   });
// });