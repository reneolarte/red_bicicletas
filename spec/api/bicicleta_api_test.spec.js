const Bicicleta = require('../../models/bicicleta');
const request = require('request');
const sever = require('../../bin/www')


beforeEach(()=> {Bicicleta.allBicis = []; });

describe('bicicleta Api', () =>{
  describe('get bicicleta /', ()=>{
    it ('status 200', ()=>{
      expect (Bicicleta.allBicis.length).toBe(0);
      
      var a = new Bicicleta(1, "negro", "urbana", [6.235234, -75.572034])
      Bicicleta.add(a);

      request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
        expect(response.statusCode).toBe(200)
      })
    })
  })
})

describe('post bicicletas /create', ()=>{
  it('status 200', (done)=>{
    var header = {'content-type': 'application/json'};
    var abici = '{"id": 1, "color":"rojo", "modelo": "urbano", "lat": -34, "lng": -54}'

    request.post({
      headers: header,
      url: 'http://localhost:3000/api/bicicletas/create',
      body: abici
    }, function(error, response, body){
      expect(response.statusCode).toBe(200);
      expect(Bicicleta.findById(1).color).toBe("rojo");
      done();
    });
  });
});

describe('post bicicletas /delete', ()=>{
  it('status 200', (done)=>{
    expect (Bicicleta.allBicis.length).toBe(0);
      
    var a = new Bicicleta(1, "negro", "urbana", [6.235234, -75.572034])
    Bicicleta.add(a);

    var header = {'content-type': 'application/json'};
    var abici = '{"id": 1}'

    request.post({
      headers: header,
      url: 'http://localhost:3000/api/bicicletas/delete',
      body: abici
    }, function(error, response, body){
      expect(response.statusCode).toBe(204);
      expect(Bicicleta.allBicis.length).toBe(0);
      done();
    });
  });
});

describe('post bicicletas /delete', ()=>{
  it('status 200', (done)=>{
    expect (Bicicleta.allBicis.length).toBe(0);
      
    var a = new Bicicleta(1, "negro", "urbana", [6.235234, -75.572034])
    Bicicleta.add(a);

    var header = {'content-type': 'application/json'};
    var abici = '{"id": 1, "color":"rojo", "modelo": "montaña", "lat": -34, "lng": -54}'

    request.post({
      headers: header,
      url: 'http://localhost:3000/api/bicicletas/update',
      body: abici
    }, function(error, response, body){
      expect(response.statusCode).toBe(200);
      expect(Bicicleta.allBicis.length).toBe(1);
      expect(Bicicleta.allBicis[0].color).toBe("rojo")
      expect(Bicicleta.allBicis[0].modelo).toBe("montaña")
      expect(Bicicleta.allBicis[0].ubicacion).toEqual([-34, -54])

      done();
    });
  });
});